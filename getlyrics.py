import syncedlyrics
import os
import pathlib
import glob
import argparse
import re
from tinytag import TinyTag

class songInfo():
    def __init__(self, filePath, defaultArtist=None, askArtistName=None, askSongName=None):
        self.filePath = filePath
        self.tag = TinyTag.get(filePath)
        self.askArtistName = askArtistName
        self.askSongName = askSongName
        self.defaultArtist = defaultArtist

    def getArtistName(self):
        if self.defaultArtist != None:
            return defaultArtist

        if self.askArtistName == True:
            print("Song path:", self.filePath, "\n")
            return input("Input artist name: ")

        return self.tag.artist

    def getSongName(self):
        if self.askSongName == True:
            print("Song path:", self.filePath, "\n")
            return input("Input song name: ")

        return self.tag.title

parser = argparse.ArgumentParser(
    prog='getLyrics',
    description='My own script for fetching lyrics. It tries to remove garbage from the song filename, and use syncedlyrics to get the lyrics in lrc.')
parser.add_argument('filename')
parser.add_argument('-f', '--force', action='store_true' , help='Will override old lyrics files')
parser.add_argument('-d', '--default-artist', action='store', type=str, help='Will use name for all songs')
parser.add_argument('-a', '--ask-artist', action='store_true', help='Will ask artist name, ignored if --default-artist is used')
parser.add_argument('-s', '--ask-song', action='store_true', help='Will ask song name')

args = parser.parse_args()
print(args)

filetypes = ('mp3', 'flac')
files=[]

if os.path.isdir(args.filename):
    for extension in filetypes:
        print(extension, os.path.join(args.filename, ('**/*.' + extension)), end="\n")
        files.extend(glob.glob(os.path.join(args.filename, ('**/*.' + extension)),recursive=True))
else:
    files = [args.filename]

for i in files:
    print(i)
    if (args.force != True) and os.path.isfile(pathlib.Path(i).with_suffix('.lrc')):
        print(pathlib.Path(i).with_suffix('.lrc'))
        print("Lyrics file already exists \n")
        continue

    song = songInfo(i,
                        defaultArtist=args.default_artist,
                        askArtistName=args.ask_artist,
                        askSongName=args.ask_song)

    print("Band: " + song.getArtistName() + "\nSong: " + song.getSongName() + "\n")
    lyrics = syncedlyrics.search(
        (song.getArtistName() + " " + song.getSongName()),
        allow_plain_format=False,
        providers=["lrclib"],
        save_path=str(pathlib.Path(i).with_suffix('.lrc')))
    if lyrics:
        print(lyrics)
    else:
        print("Lyrics not found")
